package ai.pluto.jenny;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.google.zxing.Result;
import com.orhanobut.logger.Logger;
import com.pixplicity.easyprefs.library.Prefs;
import com.tapadoo.alerter.Alerter;
import com.yanzhenjie.permission.AndPermission;
import com.yanzhenjie.permission.runtime.Permission;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

import static ai.pluto.jenny.Jenny.Settings.ASSISTANTID;
import static ai.pluto.jenny.Jenny.getContext;
import static ai.pluto.jenny.utils.Shared.clearDatabases;

public class AssistantIdentifierScannerActivity extends Activity implements ZXingScannerView.ResultHandler {
    private ZXingScannerView mScannerView;

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);

        AndPermission.with(getContext())
                .runtime()
                .permission(Permission.CAMERA)
                .onGranted(permissions -> {
                    if (permissions.contains(Permission.CAMERA))
                        Alerter.create(this)
                                .enableSwipeToDismiss()
                                .setBackgroundColorRes(R.color.colorAccent)
                                .setDuration(2500)
                                .setIcon(R.mipmap.ic_launcher_round)
                                .setText("Please scan the QR code.")
                                .showIcon(true)
                                .show();
                })
                .onDenied(permissions -> {
                    if (permissions.contains(Permission.CAMERA))
                    {
                        Toast.makeText(getContext(), R.string.camera_permission_error_msg, Toast.LENGTH_SHORT).show();

                        clearDatabases();

                        finish();
                    }
                })
                .start();

        mScannerView = new ZXingScannerView(this);
        setContentView(mScannerView);
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }

    @Override
    public void handleResult(Result rawResult) {
        try {
            Prefs.putInt(ASSISTANTID, Integer.valueOf(rawResult.getText()) );
        } catch (Exception e) {
            Logger.e(e.getMessage());
            e.printStackTrace();
        }
        Logger.d("ASSISTANT ID: " + rawResult.getText());

        if (Prefs.getInt(ASSISTANTID, -1) != -1)
            launchMainActivity();
        else {
            Alerter.create(this)
                    .enableSwipeToDismiss()
                    .setBackgroundColorRes(R.color.colorAccent)
                    .setDuration(2500)
                    .setIcon(R.mipmap.ic_launcher_round)
                    .setText("Failed to read assistant QR code, please try again.")
                    .showIcon(true)
                    .show();

            mScannerView.resumeCameraPreview(this);
        }
    }

    private void launchMainActivity() {
        Intent intent = new Intent(getContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}
