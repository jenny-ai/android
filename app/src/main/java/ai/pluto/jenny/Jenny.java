package ai.pluto.jenny;

import android.app.Application;
import android.content.Context;
import android.content.ContextWrapper;

import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;
import com.pixplicity.easyprefs.library.Prefs;

public class Jenny extends Application {

    public class Settings {
        public static final String LOGGEDIN = "logged_in";
        public static final String ASSISTANTID = "assistant_id";
        public static final String MESSAGESCOUNT = "messages_count";
        static final String REMEMBERME = "remember_me";
        static final String ACCESSTOKEN = "access_token";
        static final String NAME = "name";
        static final String EMAIL = "email";
        static final String REMINDERJENNY = "reminder_jenny";
        static final String REMINDERGOOGLE = "reminder_google";
        static final String ALARMSSNOOZENUMBER = "alarms_snooze_number";
        static final String ALARMSSNOOZEPERIOD = "alarms_snooze_period";
        static final String WEATHERLOCATIONCOUNTRY = "weather_location_country";
        static final String WEATHERLOCATIONCITY = "weather_location_city";
        static final String NEWSLOCATIONCOUNTRY = "news_location_country";
        static final String NEWSTOPICS = "news_topics";
        static final String MORNINGTEXTTOPICS = "morning_text_topics";
    }

    public class Notification {
        public static final String TAG = "jenny.companion";
        public static final int IDENTIFIER = 123;
    }

    private static Context context;

    public void onCreate() {
        super.onCreate();
        Jenny.context = getApplicationContext();

        Logger.addLogAdapter(new AndroidLogAdapter());

        new Prefs.Builder()
                .setContext(this)
                .setMode(ContextWrapper.MODE_PRIVATE)
                .setPrefsName(getPackageName())
                .setUseDefaultSharedPreference(true)
                .build();

    }

    public static Context getContext() {
        return Jenny.context;
    }
}
