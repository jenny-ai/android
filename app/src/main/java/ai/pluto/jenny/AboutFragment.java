package ai.pluto.jenny;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import mehdi.sakout.aboutpage.AboutPage;

public class AboutFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return new AboutPage(getContext())
                .isRTL(false)
                .setImage(R.mipmap.ic_launcher_round)
                .setDescription("Jenny Companion makes it so much easier to use your hotel assistant.")
                .addGroup("Contact Us")
                .addEmail("PaulaZa5@yahoo.com", "PaulaZa5@yahoo.com")
                .addWebsite("https://gitlab.com/jenny-ai", "GitLab")
                .create();
    }
}
