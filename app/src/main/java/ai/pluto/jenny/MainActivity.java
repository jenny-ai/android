package ai.pluto.jenny;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.goebl.david.Response;
import com.goebl.david.WebbException;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.hotmail.or_dvir.easysettings.pojos.CheckBoxSettingsObject;
import com.hotmail.or_dvir.easysettings.pojos.EasySettings;
import com.hotmail.or_dvir.easysettings.pojos.HeaderSettingsObject;
import com.hotmail.or_dvir.easysettings.pojos.SeekBarSettingsObject;
import com.hotmail.or_dvir.easysettings.pojos.SettingsObject;
import com.hotmail.or_dvir.easysettings_dialogs.pojos.ListSettingsObject;
import com.orhanobut.logger.Logger;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.ArrayList;
import java.util.Arrays;

import ai.pluto.jenny.utils.Communication;
import br.com.goncalves.pugnotification.notification.PugNotification;
import eu.amirs.JSON;
import me.leolin.shortcutbadger.ShortcutBadger;

import static ai.pluto.jenny.Jenny.Notification.IDENTIFIER;
import static ai.pluto.jenny.Jenny.Notification.TAG;
import static ai.pluto.jenny.Jenny.Settings.ACCESSTOKEN;
import static ai.pluto.jenny.Jenny.Settings.ALARMSSNOOZENUMBER;
import static ai.pluto.jenny.Jenny.Settings.ALARMSSNOOZEPERIOD;
import static ai.pluto.jenny.Jenny.Settings.ASSISTANTID;
import static ai.pluto.jenny.Jenny.Settings.EMAIL;
import static ai.pluto.jenny.Jenny.Settings.LOGGEDIN;
import static ai.pluto.jenny.Jenny.Settings.MORNINGTEXTTOPICS;
import static ai.pluto.jenny.Jenny.Settings.NAME;
import static ai.pluto.jenny.Jenny.Settings.NEWSLOCATIONCOUNTRY;
import static ai.pluto.jenny.Jenny.Settings.NEWSTOPICS;
import static ai.pluto.jenny.Jenny.Settings.REMEMBERME;
import static ai.pluto.jenny.Jenny.Settings.REMINDERGOOGLE;
import static ai.pluto.jenny.Jenny.Settings.REMINDERJENNY;
import static ai.pluto.jenny.Jenny.Settings.WEATHERLOCATIONCITY;
import static ai.pluto.jenny.Jenny.Settings.WEATHERLOCATIONCOUNTRY;
import static ai.pluto.jenny.Jenny.getContext;
import static ai.pluto.jenny.utils.API.doSocialLogin;
import static ai.pluto.jenny.utils.API.doSocialRegistration;
import static ai.pluto.jenny.utils.API.getSettings;
import static ai.pluto.jenny.utils.Shared.clearDatabases;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public static MainActivity main;

    public static int badgeCounter;

    public static ArrayList<SettingsObject> SETTINGS;

    public static boolean onCommandsFrag;

    private static CommandsFragment commandsFragment;

    private static TextView usernameTv;
    private static TextView emailTv;
    //wael

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //delete
        //Prefs.putInt(ASSISTANTID, 2);
        //
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        Communication.initializeMqtt();

        final String positiveBtnTxt = "Ok";
        final String negativeBtnTxt = "Cancel";
        SETTINGS = EasySettings.createSettingsArray(
                new HeaderSettingsObject.Builder("Reminders").setIcon(R.drawable.about_icon_email).build(),
                new CheckBoxSettingsObject.Builder(REMINDERJENNY, "Let Jenny remind me", true)
                        .setUseValueAsSummary()
                        .build(),
                new CheckBoxSettingsObject.Builder(REMINDERGOOGLE, "Let google remind me", false)
                        .setUseValueAsSummary()
                        .addDivider()
                        .build(),
                new HeaderSettingsObject.Builder("Alarms").build(),
                new SeekBarSettingsObject.Builder(ALARMSSNOOZENUMBER, "Number of snoozes", 0, 0, 5)
                        .setUseValueAsSummary()
                        .build(),
                new SeekBarSettingsObject.Builder(ALARMSSNOOZEPERIOD, "Snooze period", 5, 1, 15)
                        .setUseValueAsSummary()
                        .addDivider()
                        .build(),
                new HeaderSettingsObject.Builder("Weather").build(),
                new ListSettingsObject.Builder(WEATHERLOCATIONCOUNTRY, "Country", "Egypt", new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.settings_weather_countries))), positiveBtnTxt)
                        .setNegativeBtnText(negativeBtnTxt)
                        .setUseValueAsSummary()
                        .build(),
                new ListSettingsObject.Builder(WEATHERLOCATIONCITY, "City", "Cairo", new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.settings_weather_cities_egypt))), positiveBtnTxt)
                        .setNegativeBtnText(negativeBtnTxt)
                        .setUseValueAsSummary()
                        .addDivider()
                        .build(),
                new HeaderSettingsObject.Builder("News").build(),
                new ListSettingsObject.Builder(NEWSLOCATIONCOUNTRY, "Location", "Egypt", new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.settings_news_countries))), positiveBtnTxt)
                        .setNegativeBtnText(negativeBtnTxt)
                        .setUseValueAsSummary()
                        .build(),
                new ListSettingsObject.Builder(NEWSTOPICS, "Topics", ListSettingsObject.prepareValuesAsSingleString(new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.settings_news_topics)))), new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.settings_news_topics))), positiveBtnTxt)
                        .setNegativeBtnText(negativeBtnTxt)
                        .setMultiChoice()
                        .setUseValueAsSummary()
                        .addDivider()
                        .build(),
                new HeaderSettingsObject.Builder("Morning Text").build(),
                new ListSettingsObject.Builder(MORNINGTEXTTOPICS, "Topics", ListSettingsObject.prepareValuesAsSingleString(new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.settings_morning_text_topics)))), new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.settings_morning_text_topics))), positiveBtnTxt)
                        .setNegativeBtnText(negativeBtnTxt)
                        .setMultiChoice()
                        .setUseValueAsSummary()
                        .build()
        );
        EasySettings.initializeSettings(getContext(), SETTINGS);
        new GetSettingsTrialTask().execute(getContext());

        main = this;

        commandsFragment = new CommandsFragment();
        displayView(R.id.nav_commands);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        boolean r = super.onCreateOptionsMenu(menu);

        usernameTv = findViewById(R.id.username_tv);
        usernameTv.setText(Prefs.getString(NAME, ""));
        emailTv = findViewById(R.id.email_tv);
        emailTv.setText(Prefs.getString(EMAIL, ""));

        return r;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (!onCommandsFrag) {
            displayView(R.id.nav_commands);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        NavigationView navigationView = findViewById(R.id.nav_view);
        if (navigationView.getCheckedItem().getItemId() == R.id.nav_commands)
            removeBadgeAndNotification();
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        displayView(item.getItemId());

        return true;
    }

    private void displayView(int id) {

        Fragment fragment = null;
        String title = "";

        if (id == R.id.nav_commands) {
            removeBadgeAndNotification();
            fragment = commandsFragment;
            title = getString(R.string.menu_commands);
            onCommandsFrag = true;
        } else if (id == R.id.nav_room) {
            fragment = new WebviewFragment();
            fragment.setRetainInstance(true);
            title = getString(R.string.menu_room);
            onCommandsFrag = false;
        } else if (id == R.id.nav_settings) {
            new GetSettingsTrialTask().execute(getContext());
            fragment = new SettingsFragment();
            fragment.setRetainInstance(true);
            title = getString(R.string.menu_settings);
            onCommandsFrag = false;
        } else if (id == R.id.nav_about) {
            fragment = new AboutFragment();
            fragment.setRetainInstance(true);
            title = getString(R.string.menu_about);
            onCommandsFrag = false;
        } else if (id == R.id.nav_logout) {

            //wael
//            if(mGoogleSignInClient != null){
//                mGoogleSignInClient.signOut().addOnCompleteListener(this, new OnCompleteListener<Void>() {
//                    @Override
//                    public void onComplete(@NonNull Task<Void> task) {
//                        // ...
//                    }
//                });
//            }
            //boula

            clearDatabases();

            Intent intent = new Intent(getContext(), LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }

        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, fragment);
            ft.commit();
        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setCheckedItem(id);
    }

    private static void removeBadgeAndNotification() {
        badgeCounter = 0;
        ShortcutBadger.removeCount(getContext());
        PugNotification.with(getContext()).cancel(TAG, IDENTIFIER);
    }

    private class GetSettingsTrialTask extends AsyncTask<Context, Void, Void> {

        @Override
        protected Void doInBackground(Context... contexts) {
            Response<String> settings;

            try {
                settings = getSettings(Prefs.getString(ACCESSTOKEN, ""));
            } catch (WebbException e) {
                Logger.e(e.getMessage());
                e.printStackTrace();
                return null;
            }

            try {
                if (settings.getStatusCode() == 200) {
                    JSON jsonResponse = new JSON(settings.getBody());

                    Logger.d(settings.getBody());

                    Boolean reminderJenny = true;
                    if (jsonResponse.key("success").key("reminder_default_assistant").isNull())
                        reminderJenny = false;
                    else if (jsonResponse.key("success").key("reminder_default_assistant").intValue() == 0)
                        reminderJenny = false;

                    Boolean reminderGoogle = true;
                    if (jsonResponse.key("success").key("reminder_default_google_calendar").isNull())
                        reminderGoogle = false;
                    else if (jsonResponse.key("success").key("reminder_default_google_calendar").intValue() == 0)
                        reminderGoogle = false;

                    Integer alarmSnoozeNumber = jsonResponse.key("success").key("alarm_n_snooze").intValue();
                    Integer alarmSnoozePeriod = jsonResponse.key("success").key("alarm_snooze_interval").intValue();

                    String weatherLocationCountry = jsonResponse.key("success").key("weather_location_country").stringValue();
                    if (weatherLocationCountry.isEmpty())
                        weatherLocationCountry = "Egypt";

                    String weatherLocationCity = jsonResponse.key("success").key("weather_location_city").stringValue();
                    if (weatherLocationCity.isEmpty())
                        weatherLocationCity = "Cairo";

                    String newsLocationCountry = jsonResponse.key("success").key("news_location_country").stringValue();
                    if (newsLocationCountry.isEmpty())
                        newsLocationCountry = "Egypt";

                    ArrayList<String> newsTopics = new ArrayList<>();
                    if (jsonResponse.key("success").key("news_interest_sport").isNull());
                    else if (jsonResponse.key("success").key("news_interest_sport").intValue() == 0);
                    else
                        newsTopics.add("Sports");

                    if (jsonResponse.key("success").key("news_interest_business").isNull());
                    else if (jsonResponse.key("success").key("news_interest_business").intValue() == 0);
                    else
                        newsTopics.add("Business");

                    if (jsonResponse.key("success").key("news_interest_entertainment").isNull());
                    else if (jsonResponse.key("success").key("news_interest_entertainment").intValue() == 0);
                    else
                        newsTopics.add("Entertainment");

                    if (jsonResponse.key("success").key("news_interest_health").isNull());
                    else if (jsonResponse.key("success").key("news_interest_health").intValue() == 0);
                    else
                        newsTopics.add("Health");

                    if (jsonResponse.key("success").key("news_interest_science").isNull());
                    else if (jsonResponse.key("success").key("news_interest_science").intValue() == 0);
                    else
                        newsTopics.add("Science");

                    if (newsTopics.isEmpty())
                        newsTopics.add("None");

                    ArrayList<String> morningTextTopics = new ArrayList<>();
                    if (jsonResponse.key("success").key("morning_text_quote").isNull());
                    else if (jsonResponse.key("success").key("morning_text_quote").intValue() == 0);
                    else
                        morningTextTopics.add("Quotes");

                    if (jsonResponse.key("success").key("morning_text_mail").isNull());
                    else if (jsonResponse.key("success").key("morning_text_mail").intValue() == 0);
                    else
                        morningTextTopics.add("Mails");

                    if (jsonResponse.key("success").key("morning_text_news").isNull());
                    else if (jsonResponse.key("success").key("morning_text_news").intValue() == 0);
                    else
                        morningTextTopics.add("News");

                    if (jsonResponse.key("success").key("morning_text_weather").isNull());
                    else if (jsonResponse.key("success").key("morning_text_weather").intValue() == 0);
                    else
                        morningTextTopics.add("Weather");

                    if (morningTextTopics.isEmpty())
                        morningTextTopics.add("None");

                    SettingsObject remindJennySet = EasySettings.findSettingsObject(REMINDERJENNY, SETTINGS);
                    remindJennySet.setValueAndSaveSetting(contexts[0], reminderJenny);

                    SettingsObject remindGoogleSet = EasySettings.findSettingsObject(REMINDERGOOGLE, SETTINGS);
                    remindGoogleSet.setValueAndSaveSetting(contexts[0], reminderGoogle);

                    SettingsObject alarmSnoozeNumberSet = EasySettings.findSettingsObject(ALARMSSNOOZENUMBER, SETTINGS);
                    alarmSnoozeNumberSet.setValueAndSaveSetting(contexts[0], alarmSnoozeNumber);

                    SettingsObject alarmSnoozePeriodSet = EasySettings.findSettingsObject(ALARMSSNOOZEPERIOD, SETTINGS);
                    alarmSnoozePeriodSet.setValueAndSaveSetting(contexts[0], alarmSnoozePeriod);

                    SettingsObject weatherLocationCountrySet = EasySettings.findSettingsObject(WEATHERLOCATIONCOUNTRY, SETTINGS);
                    weatherLocationCountrySet.setValueAndSaveSetting(contexts[0], weatherLocationCountry);

                    SettingsObject weatherLocationCitySet = EasySettings.findSettingsObject(WEATHERLOCATIONCITY, SETTINGS);
                    weatherLocationCitySet.setValueAndSaveSetting(contexts[0], weatherLocationCity);

                    SettingsObject newsLocationCountrySet = EasySettings.findSettingsObject(NEWSLOCATIONCOUNTRY, SETTINGS);
                    newsLocationCountrySet.setValueAndSaveSetting(contexts[0], newsLocationCountry);

                    SettingsObject newsTopicsSet = EasySettings.findSettingsObject(NEWSTOPICS, SETTINGS);
                    newsTopicsSet.setValueAndSaveSetting(contexts[0], ListSettingsObject.prepareValuesAsSingleString(newsTopics));

                    SettingsObject morningTextTopicsSet = EasySettings.findSettingsObject(MORNINGTEXTTOPICS, SETTINGS);
                    morningTextTopicsSet.setValueAndSaveSetting(contexts[0], ListSettingsObject.prepareValuesAsSingleString(morningTextTopics));

                    Logger.d(
                            "Received " + JSON.create(
                                    JSON.dic(
                                            REMINDERJENNY, reminderJenny,
                                            REMINDERGOOGLE, reminderGoogle,
                                            ALARMSSNOOZENUMBER, alarmSnoozeNumber,
                                            ALARMSSNOOZEPERIOD, alarmSnoozePeriod,
                                            WEATHERLOCATIONCOUNTRY, weatherLocationCountry,
                                            WEATHERLOCATIONCITY, weatherLocationCity,
                                            NEWSLOCATIONCOUNTRY, newsLocationCountry,
                                            NEWSTOPICS, newsTopics,
                                            MORNINGTEXTTOPICS, morningTextTopics
                                    )
                            ).toString()
                    );
                }
            } catch (Exception e) {
                Logger.e(e.getMessage());
                e.printStackTrace();
            }

            return null;
        }
    }
}
