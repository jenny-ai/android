package ai.pluto.jenny;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.goebl.david.Response;
import com.goebl.david.WebbException;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.tasks.Task;
import com.orhanobut.logger.Logger;
import com.pixplicity.easyprefs.library.Prefs;

import eu.amirs.JSON;

import static ai.pluto.jenny.Jenny.Notification.TAG;
import static ai.pluto.jenny.Jenny.Settings.ACCESSTOKEN;
import static ai.pluto.jenny.Jenny.Settings.ASSISTANTID;
import static ai.pluto.jenny.Jenny.Settings.EMAIL;
import static ai.pluto.jenny.Jenny.Settings.LOGGEDIN;
import static ai.pluto.jenny.Jenny.Settings.NAME;
import static ai.pluto.jenny.Jenny.Settings.REMEMBERME;
import static ai.pluto.jenny.Jenny.getContext;
import static ai.pluto.jenny.utils.API.doLogin;
import static ai.pluto.jenny.utils.API.doSocialLogin;
import static ai.pluto.jenny.utils.API.doSocialRegistration;
import static ai.pluto.jenny.utils.API.getAccount;
import static ai.pluto.jenny.utils.API.getProfile;
import static ai.pluto.jenny.utils.Shared.clearDatabases;

public class LoginActivity extends AppCompatActivity {

    public static final String EXTRA_EMAIL = "extra_email";

    private EditText emailEt;
    private EditText passwordEt;
    private CheckBox rememberCb;
    private Button loginB;
    private Button registerB;
    private Button recoverB;

    //wael variables
    public GoogleSignInClient mGoogleSignInClient;
    public static String ibrahim_website = "https://yourbackend.example.com/tokensignin";
    public SignInButton sign_in_google;
    private static final int RC_SIGN_IN = 500;
    private static final String TAG ="failed" ;
    private static final String google_provider = "google";
    private String idToken;
    GoogleSignInAccount google_account;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        emailEt = findViewById(R.id.login_email_et);
        passwordEt = findViewById(R.id.login_password_et);
        rememberCb = findViewById(R.id.login_remember_cb);

        loginB = findViewById(R.id.login_login_btn);
        loginB.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                new LoginTrialTask().execute();
            }
        });

        registerB = findViewById(R.id.login_register_btn);
        registerB.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchRegistrationActivity();
            }
        });

        recoverB = findViewById(R.id.login_recover_btn);
        recoverB.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchRecoverActivity();
            }
        });

        if (Prefs.getBoolean(REMEMBERME, false) && (Prefs.getInt(ASSISTANTID, -1) != -1)) {
            Boolean gotProfileData = false;
            try {
                gotProfileData = new GetProfileTrialTask().execute().get();
            }
            catch (Exception e) {
                Logger.e(e.getMessage());
                e.printStackTrace();
            }

            if (gotProfileData)
                launchMainActivity();
            else {
                clearDatabases();
            }
        }
        else {
            clearDatabases();
        }

        //google sign in part by wael
        //wael
        sign_in_google = findViewById(R.id.sign_in_with_google_button);
        sign_in_google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signin_google();
            }
        });

        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).
                requestIdToken(getString(R.string.server_client_id))
                .requestEmail()
                .requestScopes(
                        new Scope(Scopes.EMAIL), new Scope(Scopes.PROFILE)
                )
                .build();        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

    }
    //boula

    private void launchMainActivity() {
        Intent intent = new Intent(getContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private void launchScannerActivity() {
        Intent intent = new Intent(getContext(), AssistantIdentifierScannerActivity.class);

        startActivity(intent);
    }

    private void launchRegistrationActivity() {
        Intent intent = new Intent(this, RegistrationActivity.class);

        if (!emailEt.getText().toString().equals("")) {
            intent.putExtra(EXTRA_EMAIL, emailEt.getText().toString());
        }

        startActivity(intent);
    }

    private void launchRecoverActivity() {
        Intent intent = new Intent(this, RecoverActivity.class);

        if (!emailEt.getText().toString().equals("")) {
            intent.putExtra(EXTRA_EMAIL, emailEt.getText().toString());
        }

        startActivity(intent);
    }

    private static void getAccountData(String accessToken) {
        Response<String> account;

        try {
            account = getAccount(accessToken);
        } catch (WebbException e) {
            Logger.e(e.getMessage());
            e.printStackTrace();
            return;
        }

        try {
            if (account.getStatusCode() == 200) {
                JSON jsonResponse = new JSON(account.getBody());

                Prefs.putString(EMAIL, jsonResponse.key("success").key("email").stringValue());
            }
        } catch (Exception e) {
            Logger.e(e.getMessage());
            e.printStackTrace();
        }
    }

    private static void getProfileData(String accessToken) {
        Response<String> profile;

        try {
            profile = getProfile(accessToken);
        } catch (WebbException e) {
            Logger.e(e.getMessage());
            e.printStackTrace();
            return;
        }

        try {
            if (profile.getStatusCode() == 200) {
                JSON jsonResponse = new JSON(profile.getBody());

                Prefs.putString(NAME, jsonResponse.key("success").key("first_name").stringValue() + " " + jsonResponse.key("success").key("last_name").stringValue());
            }
        } catch (Exception e) {
            Logger.e(e.getMessage());
            e.printStackTrace();
        }
    }

    public static void getProfileAndAccountData(String accessToken) {
        getProfileData(accessToken);
        getAccountData(accessToken);
    }

    private class GetProfileTrialTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... voids) {

            Response<String> profile;

            try {
                profile = getProfile(Prefs.getString(ACCESSTOKEN, ""));
            } catch (WebbException e) {
                Logger.e(e.getMessage());
                e.printStackTrace();
                return true;
            }

            try {
                if (profile.getStatusCode() != 401)
                    return true;
            } catch (Exception e) {
                Logger.e(e.getMessage());
                e.printStackTrace();
            }

            return false;
        }
    }

    private class LoginTrialTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loginB.setEnabled(false);
        }

        @Override
        protected Boolean doInBackground(Void... voids) {

            Response<String> response;

            try {
                response = doLogin(emailEt.getText().toString(), passwordEt.getText().toString());
            } catch (WebbException e) {
                Logger.e(e.getMessage());
                e.printStackTrace();
                return false;
            }

            try {
                if (response.getStatusCode() == 200) {
                    JSON jsonResponse = new JSON(response.getBody());

                    Prefs.clear();
                    Prefs.putBoolean(LOGGEDIN, true);
                    if (rememberCb.isChecked()) {
                        Prefs.putBoolean(REMEMBERME, true);
                    }
                    String accessToken = jsonResponse.key("success").key("token").stringValue();
                    Prefs.putString(ACCESSTOKEN, accessToken);

                    getProfileAndAccountData(accessToken);

                    return true;
                }
            } catch (Exception e) {
                Logger.e(e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean b) {
            if (b) {
                launchScannerActivity();
            }
            else {
                Toast.makeText(getContext(), R.string.login_error_msg, Toast.LENGTH_SHORT).show();
                loginB.setEnabled(true);
            }
        }
    }


    //goz2 wael l gamed

    private class LoginTrialTask_google extends AsyncTask<Void, Void, Boolean> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            sign_in_google.setEnabled(false);
        }

        @Override
        protected Boolean doInBackground(Void... voids) {

            Response<String> response;

            try {
                response = doSocialLogin(google_provider, idToken);//google wel id bta3 l user
            } catch (WebbException e) {
                Logger.e(e.getMessage());
                e.printStackTrace();
                return false;
            }

            try {
                if (response.getStatusCode() == 200) {
                    JSON jsonResponse = new JSON(response.getBody());

                    Prefs.clear();
                    Prefs.putBoolean(LOGGEDIN, true);
                    Prefs.putBoolean(REMEMBERME, true);
                    String accessToken = jsonResponse.key("success").key("token").stringValue();
                    Prefs.putString(ACCESSTOKEN, accessToken);

                    getProfileAndAccountData(accessToken);

                    return true;
                }
            } catch (Exception e) {
                Logger.e(e.getMessage());
                e.printStackTrace();
            }

            Response<String> regResponse;
            String firstName = google_account.getDisplayName() ;
            String lastName = google_account.getFamilyName();
            String gender = ""; //no gender
            String birthday = "";
            String country = "";
            String email = "";
            String provider_name = google_provider;
            String provider_id = idToken;

            try {
                regResponse = doSocialRegistration( firstName,  lastName,  gender,  birthday,  country,  email, provider_name, provider_id);
            } catch (WebbException e) {
                Logger.e(e.getMessage());
                e.printStackTrace();
                return false;
            }

            try {
                if (response.getStatusCode() == 200) {
                    JSON jsonResponse = new JSON(regResponse.getBody());

                    Prefs.clear();
                    Prefs.putBoolean(LOGGEDIN, true);
                    Prefs.putBoolean(REMEMBERME, true);
                    String accessToken = jsonResponse.key("success").key("token").stringValue();
                    Prefs.putString(ACCESSTOKEN, accessToken);

                    getProfileAndAccountData(accessToken);

                    return true;
                }
            } catch (Exception e) {
                Logger.e(e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean b) {
            if (b) {
                Intent intent = new Intent(getContext(), MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            } else {

            }
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        if (account != null){
            idToken = account.getIdToken();
            updateUI(account);
        }
    }

    private void updateUI(GoogleSignInAccount account) {
        if (account != null) {
            new LoginTrialTask_google().execute();
        }
    }


    private void signin_google() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(@NonNull Task<GoogleSignInAccount> completedTask) {
        try {
            google_account = completedTask.getResult(ApiException.class);
            idToken = google_account.getIdToken();
            // TODO(developer): send ID Token to server and validate
            updateUI(google_account);
        } catch (ApiException e) {
            Log.w(TAG, "handleSignInResult:error", e);
            updateUI(null);
        }
    }
}
