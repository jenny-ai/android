package ai.pluto.jenny;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.goebl.david.WebbException;
import com.hotmail.or_dvir.easysettings.pojos.EasySettings;
import com.orhanobut.logger.Logger;
import com.pixplicity.easyprefs.library.Prefs;

import ai.pluto.jenny.utils.Communication;
import eu.amirs.JSON;

import static ai.pluto.jenny.Jenny.Settings.ACCESSTOKEN;
import static ai.pluto.jenny.Jenny.Settings.ALARMSSNOOZENUMBER;
import static ai.pluto.jenny.Jenny.Settings.ALARMSSNOOZEPERIOD;
import static ai.pluto.jenny.Jenny.Settings.MORNINGTEXTTOPICS;
import static ai.pluto.jenny.Jenny.Settings.NEWSLOCATIONCOUNTRY;
import static ai.pluto.jenny.Jenny.Settings.NEWSTOPICS;
import static ai.pluto.jenny.Jenny.Settings.REMINDERGOOGLE;
import static ai.pluto.jenny.Jenny.Settings.REMINDERJENNY;
import static ai.pluto.jenny.Jenny.Settings.WEATHERLOCATIONCITY;
import static ai.pluto.jenny.Jenny.Settings.WEATHERLOCATIONCOUNTRY;
import static ai.pluto.jenny.MainActivity.SETTINGS;
import static ai.pluto.jenny.utils.API.pushSettings;
import static ai.pluto.jenny.utils.Communication.ASSISTANTID;
import static ai.pluto.jenny.utils.Communication.sendToAssistant;

public class SettingsFragment extends Fragment {

    private static LinearLayout settingsFrame;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.content_settings, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        settingsFrame = view.findViewById(R.id.settings_frame);
        EasySettings.inflateSettingsLayout(getContext(), settingsFrame, SETTINGS);
    }

    @Override
    public void onPause() {
        JSON settings = JSON.create(
                JSON.dic(
                        "dtype", Communication.DATA.SETTINGS,
                        "data", JSON.array(
                                JSON.dic(
                                        REMINDERJENNY, EasySettings.retrieveSettingsSharedPrefs(getContext()).getBoolean(REMINDERJENNY, false),
                                        REMINDERGOOGLE, EasySettings.retrieveSettingsSharedPrefs(getContext()).getBoolean(REMINDERGOOGLE, false),
                                        ALARMSSNOOZENUMBER, EasySettings.retrieveSettingsSharedPrefs(getContext()).getInt(ALARMSSNOOZENUMBER, 0),
                                        ALARMSSNOOZEPERIOD, EasySettings.retrieveSettingsSharedPrefs(getContext()).getInt(ALARMSSNOOZEPERIOD, 0),
                                        WEATHERLOCATIONCOUNTRY, EasySettings.retrieveSettingsSharedPrefs(getContext()).getString(WEATHERLOCATIONCOUNTRY, "Egypt"),
                                        WEATHERLOCATIONCITY, EasySettings.retrieveSettingsSharedPrefs(getContext()).getString(WEATHERLOCATIONCITY, "Cairo"),
                                        NEWSLOCATIONCOUNTRY, EasySettings.retrieveSettingsSharedPrefs(getContext()).getString(NEWSLOCATIONCOUNTRY, "Egypt"),
                                        NEWSTOPICS, JSON.array(
                                                EasySettings.retrieveSettingsSharedPrefs(getContext()).getString(NEWSTOPICS, "").split(",-,-,-,-")
                                        ),
                                        MORNINGTEXTTOPICS, JSON.array(
                                                EasySettings.retrieveSettingsSharedPrefs(getContext()).getString(MORNINGTEXTTOPICS, "").split(",-,-,-,-")
                                        )
                                )
                        ),
                        "source_type", "phone",
                        "source_id", ASSISTANTID,
                        "destination_type", "assistant",
                        "destination_id", ASSISTANTID
                )
        );

        sendToAssistant(settings);

        new PushSettingsTrialTask().execute(getContext());

        super.onPause();
    }

    private class PushSettingsTrialTask extends AsyncTask<Context, Void, Void> {

        @Override
        protected Void doInBackground(Context... contexts) {

            String reminderJenny = "0", reminderGoogle = "0";
            if (EasySettings.retrieveSettingsSharedPrefs(contexts[0]).getBoolean(REMINDERJENNY, false))
                reminderJenny = "1";
            if (EasySettings.retrieveSettingsSharedPrefs(contexts[0]).getBoolean(REMINDERGOOGLE, false))
                reminderGoogle = "1";

            String newsTopics = EasySettings.retrieveSettingsSharedPrefs(contexts[0]).getString(NEWSTOPICS, "");
            String business = "0", entertainment = "0", health = "0", science = "0", sports = "0";
            if (newsTopics.contains("Business"))
                business = "1";
            if (newsTopics.contains("Entertainment"))
                entertainment = "1";
            if (newsTopics.contains("Health"))
                health = "1";
            if (newsTopics.contains("Science"))
                science = "1";
            if (newsTopics.contains("Sports"))
                sports = "1";

            String morningTextTopics = EasySettings.retrieveSettingsSharedPrefs(contexts[0]).getString(MORNINGTEXTTOPICS, "");
            String quotes = "0", mails = "0", news = "0", weather = "0";
            if (morningTextTopics.contains("Quotes"))
                quotes = "1";
            if (morningTextTopics.contains("Mails"))
                mails = "1";
            if (morningTextTopics.contains("News"))
                news = "1";
            if (morningTextTopics.contains("Weather"))
                weather = "1";

            try {
                pushSettings(
                        Prefs.getString(ACCESSTOKEN, ""),
                        reminderJenny, reminderGoogle,
                        Integer.toString(EasySettings.retrieveSettingsSharedPrefs(contexts[0]).getInt(ALARMSSNOOZENUMBER, 0)),
                        Integer.toString(EasySettings.retrieveSettingsSharedPrefs(contexts[0]).getInt(ALARMSSNOOZEPERIOD, 0)),
                        EasySettings.retrieveSettingsSharedPrefs(contexts[0]).getString(WEATHERLOCATIONCOUNTRY, "Egypt"),
                        EasySettings.retrieveSettingsSharedPrefs(contexts[0]).getString(WEATHERLOCATIONCITY, "Cairo"),
                        EasySettings.retrieveSettingsSharedPrefs(contexts[0]).getString(NEWSLOCATIONCOUNTRY, "Egypt"),
                        sports, business, entertainment, health, science,
                        quotes, mails, news, weather
                );
            } catch (WebbException e) {
                Logger.e(e.getMessage());
                e.printStackTrace();
                return null;
            }

            Logger.d(
                    "Sent: " + JSON.create(
                            JSON.dic(
                                    REMINDERJENNY, reminderJenny,
                                    REMINDERGOOGLE, reminderGoogle,
                                    ALARMSSNOOZENUMBER, Integer.toString(EasySettings.retrieveSettingsSharedPrefs(contexts[0]).getInt(ALARMSSNOOZENUMBER, 0)),
                                    ALARMSSNOOZEPERIOD, Integer.toString(EasySettings.retrieveSettingsSharedPrefs(contexts[0]).getInt(ALARMSSNOOZEPERIOD, 0)),
                                    WEATHERLOCATIONCOUNTRY, EasySettings.retrieveSettingsSharedPrefs(contexts[0]).getString(WEATHERLOCATIONCOUNTRY, "Egypt"),
                                    WEATHERLOCATIONCITY, EasySettings.retrieveSettingsSharedPrefs(contexts[0]).getString(WEATHERLOCATIONCITY, "Cairo"),
                                    NEWSLOCATIONCOUNTRY, EasySettings.retrieveSettingsSharedPrefs(contexts[0]).getString(NEWSLOCATIONCOUNTRY, "Egypt"),
                                    NEWSTOPICS, JSON.array(
                                            sports, business, entertainment, health, science
                                    ),
                                    MORNINGTEXTTOPICS, JSON.array(
                                            quotes, mails, news, weather
                                    )
                            )
                    ).toString()
            );

            return null;
        }
    }
}
