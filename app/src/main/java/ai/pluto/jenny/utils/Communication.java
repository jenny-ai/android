package ai.pluto.jenny.utils;

import android.app.Notification;

import com.orhanobut.logger.Logger;
import com.pixplicity.easyprefs.library.Prefs;
import com.tapadoo.alerter.Alerter;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.DisconnectedBufferOptions;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import ai.pluto.jenny.Jenny;
import ai.pluto.jenny.MainActivity;
import ai.pluto.jenny.R;
import ai.pluto.jenny.utils.commands.Message;
import ai.pluto.jenny.utils.commands.User;
import br.com.goncalves.pugnotification.notification.PugNotification;
import eu.amirs.JSON;
import me.leolin.shortcutbadger.ShortcutBadger;

import static ai.pluto.jenny.CommandsFragment.messages_count;
import static ai.pluto.jenny.Jenny.Notification.IDENTIFIER;
import static ai.pluto.jenny.Jenny.Notification.TAG;
import static ai.pluto.jenny.Jenny.getContext;
import static ai.pluto.jenny.CommandsFragment.messagesAdapter;
import static ai.pluto.jenny.MainActivity.badgeCounter;
import static ai.pluto.jenny.MainActivity.main;
import static ai.pluto.jenny.MainActivity.onCommandsFrag;

public class Communication {

    public class DATA {
        public static final String COMMAND = "CM";
        public static final String SETTINGS = "ST";

        public static final String MapsUrl = "00";

        public static final String MPDTogglePlayingStatus = "0a";
        public static final String MPDNextOrPreviousSong = "0b";
        public static final String MPDSongName = "0c";
        public static final String MPDSongTime = "0d";
        public static final String MPDSeekToTime = "0e";
        public static final String MPDSongPicture = "0f";
        public static final String MPDStartMediaPlayer = "0g";
        public static final String MPDSetVolume = "0h";

        public static final String IOTTVLightIntensity = "10";
        public static final String IOTTVSoundIntensity = "11";
        public static final String IOTTVToggleStatus = "12";
        public static final String IOTLEDLightIntensity = "13";
        public static final String IOTLEDToggleStatus = "14";

        public static final String NULL = "ZZ";
    }

    public static int ASSISTANTID;
    public static String CLIENTID;
    private static String SUBSCRIBETOPIC;
    private static String PUBLISHTOPIC;
    private static String SERVER;

    private static MqttAndroidClient reactor;

    public static User assistant;

    public static void initializeMqtt() {

        ASSISTANTID = Prefs.getInt(Jenny.Settings.ASSISTANTID, -1);
        CLIENTID = "phone" + ASSISTANTID;
        SUBSCRIBETOPIC = "phones/" + ASSISTANTID;
        PUBLISHTOPIC = "assistants/" + ASSISTANTID;
        SERVER = "tcp://192.168.191.100:1883";

        reactor = new MqttAndroidClient(getContext(), SERVER, CLIENTID);
        reactor.setCallback(new MqttCallbackExtended() {
            @Override
            public void connectComplete(boolean reconnect, String serverURI) {

                if (reconnect) {
                    Logger.d("Reconnected to : " + serverURI);
                } else {
                    Logger.d("Connected to: " + serverURI);

                    try {
                        reactor.subscribe(SUBSCRIBETOPIC, 2, null, new IMqttActionListener(){
                            @Override
                            public void onSuccess(IMqttToken asyncActionToken) {
                                Logger.d("Subscribed to: " + SUBSCRIBETOPIC);
                            }

                            @Override
                            public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                                Logger.d("Failed to subscribe to: " + SUBSCRIBETOPIC);
                            }
                        });
                    } catch (MqttException e){
                        Logger.e(e.getMessage());
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void connectionLost(Throwable cause) {
                Logger.d("The mqtt connection was lost.");
                Logger.e(cause.getMessage());
            }

            @Override
            public void messageArrived(String topic, MqttMessage message) {
                Logger.d("Incoming Message: " + new String(message.getPayload()));
                handler(new String(message.getPayload()));
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken token) {
                Logger.d("Message was delivered successfully");

            }
        });

        MqttConnectOptions reactorOptions = new MqttConnectOptions();
        reactorOptions.setAutomaticReconnect(true);
        reactorOptions.setCleanSession(false);

        try {
            reactor.connect(reactorOptions, null, new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    DisconnectedBufferOptions disconnectedBufferOptions = new DisconnectedBufferOptions();
                    disconnectedBufferOptions.setBufferEnabled(true);
                    disconnectedBufferOptions.setBufferSize(100);
                    disconnectedBufferOptions.setPersistBuffer(false);
                    disconnectedBufferOptions.setDeleteOldestMessages(false);
                    reactor.setBufferOpts(disconnectedBufferOptions);
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    Logger.d(exception.getMessage());
                    exception.printStackTrace();
                }
            });
        } catch (MqttException e){
            Logger.e(e.getMessage());
            e.printStackTrace();
        }

        assistant = new User(Integer.toString(ASSISTANTID), "Assistant");
    }

    private static void handler(String message) {
        JSON msg = new JSON(message);
        String msgContent = msg.key("dtype").stringValue();
        if (msgContent.equals(DATA.COMMAND) || msgContent.equals(DATA.MapsUrl)) {
            Message chatMessage = new Message(Integer.toString(messagesAdapter.getMessagesCount()), assistant, msg.key("data").stringValue());
            messagesAdapter.addToStart(chatMessage, true);

            Prefs.putInt("messages_count", messages_count);
            Prefs.putString("message" + messages_count, msg.key("data").stringValue());
            Prefs.putString("time" + messages_count, chatMessage.getCreatedAt().toString());
            Prefs.putInt("owner" + messages_count, 1);
            messages_count++;

            boolean foregroud;
            try {
                foregroud = new ForegroundCheckTask().execute(getContext()).get();
            } catch (Exception e) {
                foregroud = false;
                Logger.e(e.getMessage());
                e.printStackTrace();
            }
            if (!foregroud) {
                badgeCounter++;
                ShortcutBadger.applyCount(getContext(), badgeCounter);
                PugNotification.with(getContext())
                        .load()
                        .identifier(IDENTIFIER)
                        .tag(TAG)
                        .title("Jenny Companion")
                        .message(msg.key("data").stringValue())
                        .number(badgeCounter)
                        .smallIcon(R.mipmap.ic_launcher_round)
                        .largeIcon(R.mipmap.ic_launcher_round)
                        .flags(Notification.DEFAULT_ALL)
                        .color(R.color.colorAccent)
                        .click(MainActivity.class)
                        .autoCancel(true)
                        .simple()
                        .build();
            }
            else if (!onCommandsFrag) {
                badgeCounter++;
                ShortcutBadger.applyCount(getContext(), badgeCounter);
                Alerter.create(main)
                        .enableSwipeToDismiss()
                        .setBackgroundColorRes(R.color.colorAccent)
                        .setDuration(2500)
                        .setIcon(R.mipmap.ic_launcher_round)
                        .setText(msg.key("data").stringValue())
                        .setTitle("New message")
                        .showIcon(true)
                        .show();
            }
        }
    }

    public static void sendToAssistant(String messageCode, String messageContent) {
        JSON message = JSON.create(
                JSON.dic(
                        "dtype", messageCode,
                        "data", messageContent,
                        "source_type", "phone",
                        "source_id", ASSISTANTID,
                        "destination_type", "assistant",
                        "destination_id", ASSISTANTID
                )
        );

        sendToAssistant(message);
    }

    public static void sendToAssistant(JSON message) {

        try {
            MqttMessage msg = new MqttMessage();
            msg.setPayload(message.toString().getBytes());
            msg.setQos(2);
            reactor.publish(PUBLISHTOPIC, msg);
            Logger.d("Message Published: " + message);
        } catch (MqttException e) {
            Logger.e(e.getMessage());
            e.printStackTrace();
        }
    }
}
