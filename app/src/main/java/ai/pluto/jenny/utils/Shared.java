package ai.pluto.jenny.utils;

import com.hotmail.or_dvir.easysettings.pojos.EasySettings;
import com.orhanobut.logger.Logger;
import com.pixplicity.easyprefs.library.Prefs;

import static ai.pluto.jenny.CommandsFragment.messagesAdapter;
import static ai.pluto.jenny.Jenny.Settings.LOGGEDIN;
import static ai.pluto.jenny.Jenny.Settings.MESSAGESCOUNT;
import static ai.pluto.jenny.Jenny.getContext;

public class Shared {

    public static void clearDatabases() {
        Prefs.clear()
                .commit();
        Prefs.putBoolean(LOGGEDIN, false);
        Prefs.putInt(MESSAGESCOUNT, -1);

        EasySettings.retrieveSettingsSharedPrefs(getContext()).edit()
                .clear()
                .commit();

        try {
            messagesAdapter.clear();
        } catch (Exception e) {
            Logger.e(e.getMessage());
            e.printStackTrace();
        }
    }
}
