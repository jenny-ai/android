package ai.pluto.jenny.utils;

import com.goebl.david.Response;
import com.goebl.david.Webb;

public final class API {

    private final static String STATIC_JENNY_API_URL = "http://192.168.191.232:80/api/";
    private final static String STATIC_JENNY_LOGIN_EXTENSION_URL = "login";
    private final static String STATIC_JENNY_RECOVER_EXTENSION_URL = "recover";
    private final static String STATIC_JENNY_REGISTRATION_EXTENSION_URL = "register";
    private final static String STATIC_JENNY_ACCOUNT_EXTENSION_URL = "account";
    private final static String STATIC_JENNY_PROFILE_EXTENSION_URL = "profile";
    private final static String STATIC_JENNY_SETTINGS_EXTENSION_URL = "settings/assistant";
    private final static String STATIC_JENNY_EDIT_SETTINGS_EXTENSION_URL = "settings/assistant/update";
    private final static String STATIC_JENNY_SOCIAL_LOGIN_EXTENSION_URL = "social-login";
    private final static String STATIC_JENNY_SOCIAL_REGISTER_EXTENSION_URL = "social-register";
    private final static String FIRST_NAME_PARAM = "first_name";
    private final static String LAST_NAME_PARAM = "last_name";
    private final static String GENDER_PARAM = "sex";
    private final static String BIRTH_DATE_PARAM = "birthdate";
    private final static String COUNTRY_PARAM = "country";
    private final static String EMAIL_PARAM = "email";
    private final static String PASSWORD_PARAM = "password";
    private final static String PASSWORD_CONFIRMATION_PARAM = "password_confirmation";
    private final static String REMINDER_JENNY_PARAM = "reminder_default_assistant";
    private final static String REMINDER_GOOGLE_PARAM = "reminder_default_google_calendar";
    private final static String ALARM_SNOOZE_NUMBER_PARAM = "alarm_n_snooze";
    private final static String ALARM_SNOOZE_PERIOD_PARAM = "alarm_snooze_interval";
    private final static String WEATHER_LOCATION_COUNTRY_PARAM = "weather_location_country";
    private final static String WEATHER_LOCATION_CITY_PARAM = "weather_location_city";
    private final static String NEWS_LOCATION_PARAM = "news_location_country";
    private final static String NEWS_TOPICS_SPORT_PARAM = "news_interest_sport";
    private final static String NEWS_TOPICS_BUSINESS_PARAM = "news_interest_business";
    private final static String NEWS_TOPICS_ENTERTAINMENT_PARAM = "news_interest_entertainment";
    private final static String NEWS_TOPICS_HEALTH_PARAM = "news_interest_health";
    private final static String NEWS_TOPICS_SCIENCE_PARAM = "news_interest_science";
    private final static String MORNING_TEXT_QUOTE_PARAM = "morning_text_quote";
    private final static String MORNING_TEXT_MAIL_PARAM = "morning_text_mail";
    private final static String MORNING_TEXT_NEWS_PARAM = "morning_text_news";
    private final static String MORNING_TEXT_WEATHER_PARAM = "morning_text_weather";
    private final static String PROVIDER_NAME_PARAM = "provider_name";
    private final static String PROVIDER_ID_PARAM = "provider_id";
    private final static String AUTHORIZATION_PARAM = "Authorization";
    private final static String ACCEPT_PARAM = "Accept";
    private final static String ACCEPT_CONTENT = "application/json";
    private final static String CONTENT_TYPE_PARAM = "Content-Type";
    private final static String CONTENT_TYPE_JSON = "application/x-www-form-urlencoded";

    public static Response<String> doLogin(String email, String password) {
        Webb webb = Webb.create();
        return webb.post(STATIC_JENNY_API_URL + STATIC_JENNY_LOGIN_EXTENSION_URL)
                .header(ACCEPT_PARAM, ACCEPT_CONTENT)
                .param(EMAIL_PARAM, email)
                .param(PASSWORD_PARAM, password)
                .ensureSuccess()
                .asString();
    }
    public static Response<String> doSocialLogin(String provider_name, String provider_id) {
        Webb webb = Webb.create();
        return webb.post(STATIC_JENNY_API_URL + STATIC_JENNY_SOCIAL_LOGIN_EXTENSION_URL)
                .header(ACCEPT_PARAM, ACCEPT_CONTENT)
                .param(PROVIDER_NAME_PARAM, provider_name)
                .param(PROVIDER_ID_PARAM, provider_id)
                .ensureSuccess()
                .asString();
    }

    public static Response<String> doRecover(String email) {
        Webb webb = Webb.create();
        return webb.post(STATIC_JENNY_API_URL + STATIC_JENNY_RECOVER_EXTENSION_URL)
                .header(ACCEPT_PARAM, ACCEPT_CONTENT)
                .param(EMAIL_PARAM, email)
                .ensureSuccess()
                .asString();
    }

    public static Response<String> doRegistration(String firstName, String lastName, String gender, String birthday, String country, String email, String password, String passwordConfirmation) {

        Webb webb = Webb.create();
        return webb.post(STATIC_JENNY_API_URL + STATIC_JENNY_REGISTRATION_EXTENSION_URL)
                .header(ACCEPT_PARAM, ACCEPT_CONTENT)
                .param(FIRST_NAME_PARAM, firstName)
                .param(LAST_NAME_PARAM, lastName)
                .param(GENDER_PARAM, gender)
                .param(BIRTH_DATE_PARAM, birthday)
                .param(COUNTRY_PARAM, country)
                .param(EMAIL_PARAM, email)
                .param(PASSWORD_PARAM, password)
                .param(PASSWORD_CONFIRMATION_PARAM, passwordConfirmation)
                .ensureSuccess()
                .asString();
    }

    public static Response<String> doSocialRegistration(String firstName, String lastName, String gender, String birthday, String country, String email,String provider_name,String provider_id) {

        Webb webb = Webb.create();
        return webb.post(STATIC_JENNY_API_URL + STATIC_JENNY_REGISTRATION_EXTENSION_URL)
                .header(ACCEPT_PARAM, ACCEPT_CONTENT)
                .param(FIRST_NAME_PARAM, firstName)
                .param(LAST_NAME_PARAM, lastName)
                .param(GENDER_PARAM, gender)
                .param(BIRTH_DATE_PARAM, birthday)
                .param(COUNTRY_PARAM, country)
                .param(EMAIL_PARAM, email)
                .param(PROVIDER_NAME_PARAM, provider_name)
                .param(provider_id , provider_id)
                .ensureSuccess()
                .asString();
    }

    public static Response<String> getAccount(String accessToken) {
        Webb webb = Webb.create();
        return webb.post(STATIC_JENNY_API_URL + STATIC_JENNY_ACCOUNT_EXTENSION_URL)
                .header(ACCEPT_PARAM, ACCEPT_CONTENT)
                .header(AUTHORIZATION_PARAM, "Bearer " + accessToken)
                .ensureSuccess()
                .asString();
    }

    public static Response<String> getProfile(String accessToken) {
        Webb webb = Webb.create();
        return webb.post(STATIC_JENNY_API_URL + STATIC_JENNY_PROFILE_EXTENSION_URL)
                .header(ACCEPT_PARAM, ACCEPT_CONTENT)
                .header(AUTHORIZATION_PARAM, "Bearer " + accessToken)
                .ensureSuccess()
                .asString();
    }

    public static Response<String> getSettings(String accessToken) {
        Webb webb = Webb.create();
        return webb.post(STATIC_JENNY_API_URL + STATIC_JENNY_SETTINGS_EXTENSION_URL)
                .header(ACCEPT_PARAM, ACCEPT_CONTENT)
                .header(AUTHORIZATION_PARAM, "Bearer " + accessToken)
                .ensureSuccess()
                .asString();
    }

    public static Response<String> pushSettings(String accessToken, String reminderJenny, String reminderGoogle, String alarmSnoozeNumber, String alarmSnoozePeriod, String weatherLocationCountry, String weatherLocationCity, String newsLocation, String newsSport, String newsBusiness, String newsEntertainment, String newsHealth, String newsScience, String morningTextQuote, String morningTextMail, String morningTextNews, String morningTextWeather) {
        Webb webb = Webb.create();
        return webb.post(STATIC_JENNY_API_URL + STATIC_JENNY_EDIT_SETTINGS_EXTENSION_URL)
                .header(ACCEPT_PARAM, ACCEPT_CONTENT)
                .header(AUTHORIZATION_PARAM, "Bearer " + accessToken)
                .param(REMINDER_JENNY_PARAM, reminderJenny)
                .param(REMINDER_GOOGLE_PARAM, reminderGoogle)
                .param(ALARM_SNOOZE_NUMBER_PARAM, alarmSnoozeNumber)
                .param(ALARM_SNOOZE_PERIOD_PARAM, alarmSnoozePeriod)
                .param(WEATHER_LOCATION_COUNTRY_PARAM, weatherLocationCountry)
                .param(WEATHER_LOCATION_CITY_PARAM, weatherLocationCity)
                .param(NEWS_LOCATION_PARAM, newsLocation)
                .param(NEWS_TOPICS_SPORT_PARAM, newsSport)
                .param(NEWS_TOPICS_BUSINESS_PARAM, newsBusiness)
                .param(NEWS_TOPICS_ENTERTAINMENT_PARAM, newsEntertainment)
                .param(NEWS_TOPICS_HEALTH_PARAM, newsHealth)
                .param(NEWS_TOPICS_SCIENCE_PARAM, newsScience)
                .param(MORNING_TEXT_QUOTE_PARAM, morningTextQuote)
                .param(MORNING_TEXT_MAIL_PARAM, morningTextMail)
                .param(MORNING_TEXT_NEWS_PARAM, morningTextNews)
                .param(MORNING_TEXT_WEATHER_PARAM, morningTextWeather)
                .ensureSuccess()
                .asString();
    }
}
