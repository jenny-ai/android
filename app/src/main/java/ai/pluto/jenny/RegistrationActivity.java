package ai.pluto.jenny;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.goebl.david.Response;
import com.goebl.david.WebbException;
import com.orhanobut.logger.Logger;
import com.pixplicity.easyprefs.library.Prefs;

import eu.amirs.JSON;

import static ai.pluto.jenny.Jenny.Settings.ACCESSTOKEN;
import static ai.pluto.jenny.Jenny.Settings.LOGGEDIN;
import static ai.pluto.jenny.Jenny.Settings.REMEMBERME;
import static ai.pluto.jenny.Jenny.getContext;
import static ai.pluto.jenny.LoginActivity.EXTRA_EMAIL;
import static ai.pluto.jenny.LoginActivity.getProfileAndAccountData;
import static ai.pluto.jenny.utils.API.doRegistration;

public class RegistrationActivity extends AppCompatActivity {

    private EditText firstNameEt;
    private EditText lastNameEt;
    private Spinner gender;
    private EditText birthdayEt;
    private Spinner country;
    private EditText emailEt;
    private EditText passwordEt;
    private EditText repeatPasswordEt;
//    private CheckBox acceptRulesCb;
    private Button registerB;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        Intent intent = getIntent();
        if (intent == null) {
            closeOnError();
        }

        firstNameEt = findViewById(R.id.register_firstname_et);
        lastNameEt = findViewById(R.id.register_lastname_et);

        gender = findViewById(R.id.register_gender_sp);
        ArrayAdapter<CharSequence> genderAdapter = ArrayAdapter.createFromResource(this, R.array.register_gender_items, android.R.layout.simple_spinner_item);
        gender.setAdapter(genderAdapter);

        birthdayEt = findViewById(R.id.register_birthday_et);

        country = findViewById(R.id.register_country_sp);
        ArrayAdapter<CharSequence> countryAdapter = ArrayAdapter.createFromResource(this, R.array.register_country_items, android.R.layout.simple_spinner_item);
        country.setAdapter(countryAdapter);

        String email = "";
        try {
            email = intent.getStringExtra(EXTRA_EMAIL);
        }
        catch (NullPointerException e) {
            Logger.e(e.getMessage());
            e.printStackTrace();
        }

        emailEt = findViewById(R.id.register_email_et);
        emailEt.setText(email);

        passwordEt = findViewById(R.id.register_password_et);
        repeatPasswordEt = findViewById(R.id.register_repeat_password_et);
//        acceptRulesCb = (CheckBox) findViewById(R.id.register_accept_rules);

        registerB = findViewById(R.id.register_register_btn);
        registerB.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                new RegistrationTrialTask().execute();
            }
        });
    }

    private void closeOnError() {
        finish();
        Toast.makeText(this, R.string.generic_error_msg, Toast.LENGTH_SHORT).show();
    }

    private class RegistrationTrialTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            registerB.setEnabled(false);
        }

        @Override
        protected Boolean doInBackground(Void... voids) {

            Response<String> response;

            try {
                String genderTxt = gender.getSelectedItem().toString();
                String selectedGender = "0";

                if (genderTxt.equals("Intersex"))
                    selectedGender = "0";
                else if (genderTxt.equals("Male"))
                    selectedGender = "1";
                else if (genderTxt.equals("Female"))
                    selectedGender = "2";
                else if (genderTxt.equals("Undetermined"))
                    selectedGender = "9";

                response = doRegistration(firstNameEt.getText().toString(), lastNameEt.getText().toString(), selectedGender, birthdayEt.getText().toString(), country.getSelectedItem().toString(), emailEt.getText().toString(), passwordEt.getText().toString(), repeatPasswordEt.getText().toString());
            } catch (WebbException e) {
                Logger.e(e.getMessage());
                e.printStackTrace();
                return false;
            }

            try {
                if (response.getStatusCode() == 200) {
                    JSON jsonResponse = new JSON(response.getBody());

                    Prefs.clear();
                    Prefs.putBoolean(LOGGEDIN, true);
                    Prefs.putBoolean(REMEMBERME, true);
                    String accessToken = jsonResponse.key("success").key("token").stringValue();
                    Prefs.putString(ACCESSTOKEN, accessToken);

                    getProfileAndAccountData(accessToken);

                    return true;
                }
            } catch (Exception e) {
                Logger.e(e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean b) {
            if (b) {
                launchScannerActivity();
            }
            else {
                Toast.makeText(getContext(), R.string.register_error_msg, Toast.LENGTH_SHORT).show();
                registerB.setEnabled(true);
            }
        }
    }

    private void launchScannerActivity() {
        Intent intent = new Intent(getContext(), AssistantIdentifierScannerActivity.class);

        startActivity(intent);
    }
}
