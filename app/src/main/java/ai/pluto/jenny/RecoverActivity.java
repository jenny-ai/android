package ai.pluto.jenny;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.goebl.david.Response;
import com.goebl.david.WebbException;
import com.orhanobut.logger.Logger;

import static ai.pluto.jenny.Jenny.getContext;
import static ai.pluto.jenny.LoginActivity.EXTRA_EMAIL;
import static ai.pluto.jenny.utils.API.doRecover;

public class RecoverActivity extends AppCompatActivity {

    private EditText emailEt;
    private Button recoverB;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recover);

        Intent intent = getIntent();
        if (intent == null) {
            closeOnError();
        }

        String email = "";
        try {
            email = intent.getStringExtra(EXTRA_EMAIL);
        }
        catch (NullPointerException e) {
            Logger.e(e.getMessage());
            e.printStackTrace();
        }

        emailEt = findViewById(R.id.recover_email_et);
        emailEt.setText(email);

        recoverB = findViewById(R.id.recover_recover_btn);
        recoverB.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                new RecoverTrialTask().execute();
            }
        });
    }

    private void closeOnError() {
        finish();
        Toast.makeText(this, R.string.generic_error_msg, Toast.LENGTH_SHORT).show();
    }

    private class RecoverTrialTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            recoverB.setEnabled(false);
        }

        @Override
        protected Boolean doInBackground(Void... voids) {

            Response<String> response;

            try {
                response = doRecover(emailEt.getText().toString());
            } catch (WebbException e) {
                Logger.e(e.getMessage());
                e.printStackTrace();
                return false;
            }

            try {
                return response.getStatusCode() == 200;
            } catch (Exception e) {
                Logger.e(e.getMessage());
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean b) {
            if (b) {
                finish();
                Toast.makeText(getContext(), R.string.recover_success_msg, Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(getContext(), R.string.recover_error_msg, Toast.LENGTH_SHORT).show();
                recoverB.setEnabled(true);
            }
        }
    }
}
