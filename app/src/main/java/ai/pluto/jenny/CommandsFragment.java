package ai.pluto.jenny;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pixplicity.easyprefs.library.Prefs;
import com.stfalcon.chatkit.messages.MessageInput;
import com.stfalcon.chatkit.messages.MessagesList;
import com.stfalcon.chatkit.messages.MessagesListAdapter;

import java.util.Date;
import java.util.List;
import java.util.Vector;

import ai.pluto.jenny.utils.Communication;
import ai.pluto.jenny.utils.commands.Message;
import ai.pluto.jenny.utils.commands.User;

import static ai.pluto.jenny.Jenny.Settings.MESSAGESCOUNT;
import static ai.pluto.jenny.utils.Communication.assistant;

public class CommandsFragment extends Fragment {

    private static MessagesList commandsView;
    public static MessagesListAdapter<Message> messagesAdapter;
    private MessageInput commandsInput;
    private static MessageInput.InputListener inputListner;
    private static User phone;
    private static boolean initialized = false;
    public static int messages_count;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        messages_count = Prefs.getInt(MESSAGESCOUNT, -1) + 1;
        return inflater.inflate(R.layout.content_commands, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        commandsView = view.findViewById(R.id.commands_view);
        commandsInput = view.findViewById(R.id.commands_input);

        if (!initialized) {
            phone = new User(Communication.CLIENTID, "phone");

            messagesAdapter = new MessagesListAdapter<>(Communication.CLIENTID, null);

            List<Message> messages = new Vector<>();
            int i = messages_count - 1;
            int lim = -1;
            while (i > lim) {
                if (Prefs.getInt("owner" + i, -1) == 0)
                {
                    Message chatMessage = new Message(Integer.toString(i), phone, Prefs.getString("message" + i, ""), new Date(Prefs.getString("time" + i, "")));
                    messages.add(chatMessage);
                }
                else {
                    Message chatMessage = new Message(Integer.toString(i), assistant, Prefs.getString("message" + i, ""), new Date(Prefs.getString("time" + i, "")));
                    messages.add(chatMessage);
                }
                i--;
            }
            messagesAdapter.addToEnd(messages, false);

            inputListner = new MessageInput.InputListener() {
                @Override
                public boolean onSubmit(CharSequence input) {
                    Communication.sendToAssistant(Communication.DATA.COMMAND, input.toString());

                    Message chatMessage = new Message(Integer.toString(messages_count), phone, input.toString());
                    messagesAdapter.addToStart(chatMessage, true);

                    Prefs.putInt(MESSAGESCOUNT, messages_count);
                    Prefs.putString("message" + messages_count, input.toString());
                    Prefs.putString("time" + messages_count, chatMessage.getCreatedAt().toString());
                    Prefs.putInt("owner" + messages_count, 0);
                    messages_count++;

                    return true;
                }
            };
            initialized = true;
        }

        commandsView.setAdapter(messagesAdapter);
        commandsInput.setInputListener(inputListner);
    }
}
